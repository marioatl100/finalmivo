package deloitte.academy.entity;

/**
 * Clase abstracta Producto con atributos codigo, cantidad, precio y nombre
 * 
 * @version 1
 * @author mvillegas
 *
 */
public abstract class Producto {
	public Codigos codigo;
	public int cantidad;
	public double precio;
	public String nombre;

	/**
	 * Constructor con todos los parametros
	 * 
	 * @param codigo
	 * @param cantidad
	 * @param precio
	 * @param nombre
	 */
	public Producto(Codigos codigo, int cantidad, double precio, String nombre) {
		super();
		this.codigo = codigo;
		this.cantidad = cantidad;
		this.precio = precio;
		this.nombre = nombre;
	}

	/**
	 * Constructor con clase Producto como parametro
	 * 
	 * @param p
	 */
	public Producto(Producto p) {
		super();
		this.codigo = p.codigo;
		this.cantidad = p.cantidad;
		this.precio = p.precio;
		this.nombre = p.nombre;
	}

	/**
	 * Constroctor Producto sin parametros
	 */
	public Producto() {
		// TODO Auto-generated constructor stub
	}

	public Codigos getCodigo() {
		return codigo;
	}

	public void setCodigo(Codigos codigo) {
		this.codigo = codigo;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public abstract void vender();

	@Override
	public abstract String toString();

	public abstract void create();

	public abstract void read();

	public abstract void update(Producto cambio);

	public abstract void delete();

}
