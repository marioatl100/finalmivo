package deloitte.academy.machine;

import java.util.List;

import deloitte.academy.entity.Articulo;
import deloitte.academy.run.Main;

public class Funciones {
	/**
	 * Recibe una lista con articulos para imprimirla
	 * @version 1
	 * @param lista
	 */
	public static void imprime(List<Articulo> lista) {
		System.out.println("LISTA PRODUCTOS");
		for (Articulo aux : lista) {
			aux.read();
			System.out.println("---------------------------------------------");
		}
		System.out.println("---------------------------------------------");

	}
	/**
	 * Recibe un codigo en modo de String 
	 * para regresar el articulo que se encuentre con ese codigo en la lista
	 * en caso de no encontrar nada regresa un articulo con atributos nulos
	 * @param codigo
	 * @code articulo = new Articulo(aux);
	 * @return clase Articulo
	 */
	public static Articulo buscar(String codigo) {
		
		Articulo articulo = new Articulo();
		try {
		for(Articulo aux: Main.stock) 
		{
			if(aux.codigo.toString().equals(codigo)) 
			{
				 articulo = new Articulo(aux);
			}
		}}
		catch(Exception e) 
		{
			System.out.println("Error");
		}
		return articulo;
		
		}
	/**
	 * Funcion que recibe un codigo en modo de String  
	 * para simular una venta
	 * @see buscar();
	 * @param codigo
	 */
	public static void vender(String codigo) {
		
			buscar(codigo).vender();
	}
	
}
