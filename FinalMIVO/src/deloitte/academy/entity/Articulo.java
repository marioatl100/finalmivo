package deloitte.academy.entity;

import java.util.logging.Logger;

import deloitte.academy.run.Main;

/**
 * Clase Articulo que hereda atributos y metodos de la clase Producto
 * 
 * @version 1
 * @author mvillegas
 *
 */
public class Articulo extends Producto {
	private static final Logger LOGGER = Logger.getLogger(Articulo.class.getName());;

	/**
	 * Funcion override que simula una venta disminuyendo en 1na unidad el articulo
	 * seleccionado y agregandolo a la lista de ventas
	 */
	@Override
	public void vender() {
		int contador = 0;
		if (this.getCantidad() > 0) {
			try {
				for (Articulo aux : Main.stock) {
					if (this.getCodigo().equals(aux.getCodigo())) {
						aux.setCantidad((aux.getCantidad() - 1));
						Main.stock.set(contador, aux);
						LOGGER.info("Producto vendido");
					}
					contador++;
				}
				Articulo aux2 = new Articulo(this);
				aux2.setCantidad(1);
				Main.vendidos.add(aux2);
			} catch (Exception e) {
				LOGGER.severe("Error");
			}
		} else {

			LOGGER.info("Producto agotado o inexistente");
		}

	}

	/**
	 * Constructor sin parametros
	 */
	public Articulo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con todos los parametros
	 * 
	 * @param codigo
	 * @param cantidad
	 * @param precio
	 * @param nombre
	 */
	public Articulo(Codigos codigo, int cantidad, double precio, String nombre) {
		super(codigo, cantidad, precio, nombre);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor con clase Producto como parametro
	 * 
	 * @param p
	 */
	public Articulo(Producto p) {
		super(p);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Codigo: " + this.codigo.toString() + "\nCantidad: " + this.cantidad + "\nPrecio: $" + precio
				+ "\nNombre: " + this.nombre;
	}

	/**
	 * Metodo que agrega un articulo previamente creado al stock
	 */
	@Override
	public void create() {
		try {
			if (Main.stock.contains(this)) {
				LOGGER.info("Ese producto ya existe");

			} else {
				Main.stock.add(this);
			}
		} catch (Exception e) {
			LOGGER.severe("Error");
		}
	}

	/**
	 * Funcion override que imprime todos los atributos de un articulo
	 */
	@Override
	public void read() {

		System.out.println(this.toString());
	}

	/**
	 * Metodo Override que actualiza en el stock un producto
	 */
	@Override
	public void update(Producto cambio) {
		try {
			int contador = 0;

			for (Articulo aux2 : Main.stock) {

				if (this.getCodigo().equals(aux2.getCodigo())) {
					Articulo aux = new Articulo(cambio);
					Main.stock.set(contador, aux);
					LOGGER.info("Producto actualizado");

				}
				contador++;
			}
		} catch (Exception e) {
			LOGGER.severe("Error");

		}

	}

	/**
	 * Funcion override que elimina los atributos del stock de un producto deseado
	 * dejandolos como nulos
	 * 
	 */
	@Override
	public void delete() {
		try {
			int contador = 0;
			for (Articulo aux : Main.stock) {
				if (this.getCodigo().equals(aux.getCodigo())) {
					Articulo aux2 = new Articulo(this.codigo, 0, 0, "");
					Main.stock.set(contador, aux2);
					LOGGER.info("Producto eliminado");

				}
				contador++;
			}
		} catch (Exception e) {
			LOGGER.severe("Error");
		}

	}

}
