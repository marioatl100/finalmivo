package deloitte.academy.run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.entity.Articulo;
import deloitte.academy.entity.Codigos;
import deloitte.academy.machine.Funciones;

/**
 * Clase main donde se lleva acabo toda la implementacion de la maquina
 * @throws Exception
 * 
 * @author mvillegas
 *
 */
public class Main {
	public static List<Articulo> stock = new ArrayList<Articulo>();
	public static List<Articulo> vendidos = new ArrayList<Articulo>();
	private static final Logger LOGGER = Logger.getLogger(Main.class.getName());;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		Articulo a1 = new Articulo(Codigos.A1, 10, 10.50, "Chocolate");
		Articulo a2 = new Articulo(Codigos.A2, 4, 15.50, "Doritos");
		Articulo a3 = new Articulo(Codigos.A3, 2, 22.50, "Coca-Cola");
		Articulo a4 = new Articulo(Codigos.A4, 6, 8.75, "Gomitas");
		Articulo a6 = new Articulo(Codigos.A6, 2, 15, "Jugo");
		Articulo a5 = new Articulo(Codigos.A5, 10, 30, "Chips");
		Articulo b1 = new Articulo(Codigos.B1, 2, 10, "Galletas");
		Articulo b2 = new Articulo(Codigos.B2, 6, 120, "Canelitas");
		Articulo b3 = new Articulo(Codigos.B3, 10, 10.10, "Halls");
		Articulo b4 = new Articulo(Codigos.B4, 10, 3.14, "Tarta");
		Articulo b5 = new Articulo(Codigos.B5, 0, 15.55, "Sabritas");
		Articulo b6 = new Articulo(Codigos.B6, 4, 12.25, "Cheetos");
		Articulo c1 = new Articulo(Codigos.C1, 1, 10, "Rocaleta");
		Articulo c2 = new Articulo(Codigos.C2, 6, 14.75, "Rancheritos");
		Articulo c3 = new Articulo(Codigos.C3, 10, 13.15, "Ruffles");
		Articulo c4 = new Articulo(Codigos.C4, 9, 22, "Pizza fr�a");
		int opcion = 0;
		a1.create();
		//a1.create();
		a2.create();
		a3.create();
		a4.create();
		a5.create();
		a6.create();
		b1.create();
		b2.create();
		b3.create();
		b4.create();
		b5.create();
		b6.create();
		c1.create();
		c2.create();
		c3.create();
		c4.create();

		// Funciones.imprime(stock);
		 Funciones.buscar("A5").read();
		
		try {

			while (opcion != 9) {

				System.out.println("Que desea realizar");
				System.out.println("1.-Comprar");
				System.out.println("2.-Actualizar");
				System.out.println("3.-Eliminar");
				System.out.println("4.-Ver ventas");
				System.out.println("5.-Ver total de ventas");
				System.out.println("6.-Imprimir articulos");
				System.out.println("9.-Salir");
				opcion = Integer.parseInt(br.readLine());
				switch (opcion) {

				case 1:
					System.out.println("Ingresa el codigo del producto a comprar");
					String a = br.readLine();
					Funciones.vender(a);

					break;
				case 2:
					try {
						System.out.println("Ingrese el nuevo nombre: ");
						String name = br.readLine();
						System.out.println("Ingrese cantidad");
						int cantidad = Integer.parseInt(br.readLine());
						System.out.println("Ingrese precio");
						double precio = Double.parseDouble(br.readLine());
						System.out.println("Ingrese codigo existente ");
						// System.out.println("1.-A1\n 2.-A2\n 3.-A3\n 4.-A4\n 5.-A5\n 6.-A6\n 7.-B1\n
						// 8.-B2\n 9.-B3\n 10.-B4\n 11.-B5\n 12.-B6\n 13.-C1\n 14.-C2\n 15.-C3\n
						// 16.-C4\n");
						String codigo = br.readLine();
						Articulo cambio = new Articulo(Funciones.buscar(codigo).codigo, cantidad, precio, name);

						cambio.update(cambio);
					} catch (Exception e) {
						LOGGER.severe("Error");
					}
					break;
				case 3:
					System.out.println("Ingresa el codigo del articulo a borrar");
					String codigo = br.readLine();
					Funciones.buscar(codigo).delete();
					break;
				case 4:
					Funciones.imprime(vendidos);

					break;
				case 5:
					System.out.println("Todal de ventas: " + vendidos.size());

					break;
				case 6:
					Funciones.imprime(stock);

					break;
				case 9:
					System.out.println("Adios!");
					break;
				default:
					System.out.println("Opci�n no v�lida");
					break;
				}

			}
		} catch (Exception e) {
			LOGGER.severe("Error");

		}

	}

}
