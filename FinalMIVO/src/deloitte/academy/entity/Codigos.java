package deloitte.academy.entity;
/**
 * Enum que contiene los Codigos de los articulos
 * @version 1
 * @author mvillegas
 *
 */
public enum Codigos {
	A1, A2, A3, A4, A5, A6, B1, B2, B3, B4, B5, B6, C1, C2, C3, C4;

}
